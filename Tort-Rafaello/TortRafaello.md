<!-- 
Title:    Tort Rafaello, pzepis Oka
Author:   Marta 'Oko' Krawczyk, Marcin J. Kraśny 
Date:     26.01.2018 
Comment:  
-->

----
# Tort Rafaello wg. Oka
----

### *wersja z waflami*
*Zalecałbym jednak użycie spodów tortowych, tak jak w oryginalnym przepisie Oka (MJK)*

------
# Składniki
- [✓] tłusta śmietanka ok. 30-35%; 500ml
- [✓] biała czekolada; 100 g
- [✓] bita śmietana w proszku (aka 'śnieżka'); x3 (150g)
- [✓] wiórki kokosowe; 60 g
- [✓] migdały w płatkach; 30 g
- [✓] torebka herbaty
- [✓] spody tortowe 3 sztuki (w tej wersji wafle 5x)

  <!-- > img:1-4 -->

<!-- ![Wszystkie potrzebne składniki](img/01.jpeg "Wszystkie składniki") -->
<img src="img/thumbnails/01.jpg" alt="Wszystkie potrzebne składniki" width="400"/>

<!-- <img src="img/01.jpeg" width="120" height="120"> -->

<!-- <img src="img/thumbnails/02.jpg" alt="Śmietanka" width="200"/>
<img src="img/thumbnails/03.jpeg" alt="Wiorki kokosowe" width="300"/>
<img src="img/thumbnails/04.jpeg" alt="Płatki migdałów" width="300"/> -->



# Przygotowanie
****
1. Zaparz pół filiżanki (ok. 100 ml) mocnej herbaty.

2. Śmietanę (500ml) i bitą śmietanę w proszku (3 torebki) dodaj do jednej miski.

> <img src="img/thumbnails/05.jpg" alt="Miksujemy!" width="300"/>

3. Ubij mikserem na sztywna masę.
(ok 5 min, na początku wolne obroty, zwiększaj stopniowo do maksymalnych)

> <img src="img/thumbnails/06.jpg" alt="ok 5 min. ubijania" width="300"/>
> <img src="img/thumbnails/07.jpg" alt="Finalna konsystencja" width="300"/>

4. Roztop białą czekoladę w kąpieli wodnej.

> <img src="img/thumbnails/08.jpg" alt="Czekolada w kąpieli wodnej" width="300"/>
> <img src="img/thumbnails/09.jpg" alt="Roztopiona czekolada" width="300"/>

5. Dodaj wiórki kokosowe (35g) i roztopioną białą czekoladę do uprzednio ubitej masy (pt. 3).

> <img src="img/thumbnails/10.jpg" alt="Dodaj 35g wiórek kokosowych" width="300"/>
> <img src="img/thumbnails/11.jpg" alt="Dodaj roztopiną czekoladę" width="300"/>

6. Zmiksuj wszystko na jednolitą masę (ok 3 min na średnich obrotach).

> <img src="img/thumbnails/12.jpg" alt="Miksuj do jednolitej masy" width="300"/>

7. Następnie weź biszkopt i nasączasz go czajówą.

> <img src="img/thumbnails/13.jpg" alt="Nasącz biszkopt herbatą" width="300"/>

8. Nałóż krem.

> <img src="img/thumbnails/14.jpg" alt="Nałóż krem" width="300"/>

9. Następny biszkopt , czajówa, krem i biszkopt.

> <img src="img/thumbnails/15.jpg" alt="Powtarzaj przez wszystkie warstwy" width="300"/>

10. Na koniec, pozostałego kremu używasz do przykrycia zewnętrznego.

> <img src="img/thumbnails/16.jpg" alt="Obsmaruj szystko dookoła" width="300"/>

11. Górę przyozdabiasz migdałami (30 g) i resztą wiórek (25 g).

> <img src="img/thumbnails/17.jpg" alt="Przyprusz wiórkami i migdałami" width="300"/>
> <img src="img/thumbnails/18.jpg" alt="Gotowe" width="300"/>

12. Odstaw do lodówki aż krem stężeje (ok 2 h)

# Propozycja podania

> <img src="img/thumbnails/19.jpg" alt="Na talerzu" width="300"/>


   ### Smacznego!

---
